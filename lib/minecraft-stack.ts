import * as cdk from 'aws-cdk-lib'
import { Construct } from 'constructs'
// import * as sqs from 'aws-cdk-lib/aws-sqs';

export class MinecraftStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props)

    // create an ec2 instance with 2gb of ram

    const backupBucket = new cdk.aws_s3.Bucket(this, 'MinecraftBackupBucket', {
      removalPolicy: cdk.RemovalPolicy.DESTROY,
      autoDeleteObjects: true
    })

    const ec2RoleForBucketAccess = new cdk.aws_iam.Role(this, 'MinecraftEC2RoleForBucketAccess', {
      assumedBy: new cdk.aws_iam.ServicePrincipal('ec2.amazonaws.com')
    })

    backupBucket.grantReadWrite(ec2RoleForBucketAccess)

    const vpc = new cdk.aws_ec2.Vpc(this, 'MinecraftVPC', {
      maxAzs: 1,
      natGateways: 0,
      subnetConfiguration: [
        {
          name: 'minecraft',
          subnetType: cdk.aws_ec2.SubnetType.PUBLIC
        }
      ]
    })

    const securityGroup = new cdk.aws_ec2.SecurityGroup(this, 'MinecraftSecurityGroup', {
      vpc,
      allowAllOutbound: true
    })

    securityGroup.addIngressRule(cdk.aws_ec2.Peer.anyIpv4(), cdk.aws_ec2.Port.tcp(25565))
    securityGroup.addIngressRule(cdk.aws_ec2.Peer.anyIpv4(), cdk.aws_ec2.Port.udp(25565))

    securityGroup.addIngressRule(cdk.aws_ec2.Peer.anyIpv4(), cdk.aws_ec2.Port.tcp(22))

    const key = new cdk.aws_ec2.CfnKeyPair(this, 'MinecraftKeyPair', {
      keyName: 'minecraft',
      publicKeyMaterial: process.env.PUBLIC_SSH_KEY
    })

    const vm = new cdk.aws_ec2.Instance(this, 'MinecraftInstance', {
      instanceType: new cdk.aws_ec2.InstanceType('t3a.medium'),
      machineImage: new cdk.aws_ec2.AmazonLinuxImage(),
      vpc,
      securityGroup,
      keyName: key.keyName,
      // role: ec2RoleForBucketAccess,
      userData: cdk.aws_ec2.UserData.custom(`#!/bin/bash
      mkdir /home/Minecraft
      cd /home/Minecraft
      echo 'Downloading Java'
      wget https://download.oracle.com/java/18/archive/jdk-18.0.2.1_linux-x64_bin.rpm
      rpm -ivh jdk-18.0.2.1_linux-x64_bin.rpm
      java --version
      echo 'Downloading Minecraft Server'
      wget https://piston-data.mojang.com/v1/objects/84194a2f286ef7c14ed7ce0090dba59902951553/server.jar
      java -Xmx1024M -Xms1024M -jar server.jar nogui
      sed -i 's/false/true/g' eula.txt
      java -Xmx1024M -Xms1024M -jar server.jar nogui`)
    })

    new cdk.CfnOutput(this, 'MinecraftInstancePublicIp', {
      value: vm.instancePublicIp
    })
  }
}
